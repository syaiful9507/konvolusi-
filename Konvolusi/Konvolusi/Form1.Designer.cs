﻿namespace Konvolusi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnload = new System.Windows.Forms.Button();
            this.btngrayscale = new System.Windows.Forms.Button();
            this.btnfilter4 = new System.Windows.Forms.Button();
            this.btnfilter8 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnload
            // 
            this.btnload.Location = new System.Drawing.Point(23, 12);
            this.btnload.Name = "btnload";
            this.btnload.Size = new System.Drawing.Size(93, 37);
            this.btnload.TabIndex = 0;
            this.btnload.Text = "LOAD";
            this.btnload.UseVisualStyleBackColor = true;
            this.btnload.Click += new System.EventHandler(this.btnload_Click);
            // 
            // btngrayscale
            // 
            this.btngrayscale.Location = new System.Drawing.Point(136, 12);
            this.btngrayscale.Name = "btngrayscale";
            this.btngrayscale.Size = new System.Drawing.Size(95, 37);
            this.btngrayscale.TabIndex = 1;
            this.btngrayscale.Text = "GrayScale";
            this.btngrayscale.UseVisualStyleBackColor = true;
            this.btngrayscale.Click += new System.EventHandler(this.btngrayscale_Click);
            // 
            // btnfilter4
            // 
            this.btnfilter4.Location = new System.Drawing.Point(258, 12);
            this.btnfilter4.Name = "btnfilter4";
            this.btnfilter4.Size = new System.Drawing.Size(94, 37);
            this.btnfilter4.TabIndex = 2;
            this.btnfilter4.Text = "Filter 4 Node";
            this.btnfilter4.UseVisualStyleBackColor = true;
            this.btnfilter4.Click += new System.EventHandler(this.btnfilter4_Click);
            // 
            // btnfilter8
            // 
            this.btnfilter8.Location = new System.Drawing.Point(376, 12);
            this.btnfilter8.Name = "btnfilter8";
            this.btnfilter8.Size = new System.Drawing.Size(93, 37);
            this.btnfilter8.TabIndex = 3;
            this.btnfilter8.Text = "Filter 8 Node";
            this.btnfilter8.UseVisualStyleBackColor = true;
            this.btnfilter8.Click += new System.EventHandler(this.btnfilter8_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(23, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(286, 279);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(335, 65);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(283, 279);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 358);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnfilter8);
            this.Controls.Add(this.btnfilter4);
            this.Controls.Add(this.btngrayscale);
            this.Controls.Add(this.btnload);
            this.Name = "Form1";
            this.Text = "Andreas";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnload;
        private System.Windows.Forms.Button btngrayscale;
        private System.Windows.Forms.Button btnfilter4;
        private System.Windows.Forms.Button btnfilter8;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

